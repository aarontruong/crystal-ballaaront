package android.truonga.crystalball;

public class Predictions {

    private static Predictions predictions;
    private String[] answers;
    public int randomNumber;

    private Predictions() {
        answers = new String[]{
                "Your wishes will come true",
                "Mr.Al is stalking you",
                "You will find a dollar",
                "Someone will steal your microwave",
                "You are currently reading this text",
                "You will meet your TRUE love",
                "If you add 1+1 the answer is 2",
                "You will receive a fortune",
                "You will be hungry again in an hour",
                "When you drink water, you will pee",
                "Life is great, until you die",
                "Always use a condom",
        };
    }

    public static Predictions get() {
        if(predictions == null) {
            predictions = new Predictions();
        }
        return predictions;
    }

    public String getPredictions(int randomNumber) {
        return answers[randomNumber];
    }

}

